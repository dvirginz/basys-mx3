#include "config.h"
#include "lcd.h"
#include "led.h"
#include "ssd.h"
#include "utils.h"
#include "clock.h"
//#include "timed.h"
#include "utils.h"
#include "rgbled.h"
#include "btn.h"
#include <xc.h>  
#include "audio.h"
#include <sys/attribs.h>
#include <string.h>
     
//==================================================
//              Global Configuration
//==================================================
// Device Config Bits in  DEVCFG1:	
//#pragma config FNOSC =	FRCPLL

#pragma config FNOSC = PRIPLL
#pragma config POSCMOD = XT
#pragma config FSOSCEN =	OFF
//#pragma config POSCMOD =	EC
#pragma config OSCIOFNC =	ON
#pragma config FPBDIV =     DIV_1

// Device Config Bits in  DEVCFG2:	
#pragma config FPLLIDIV =	DIV_2
#pragma config FPLLMUL =	MUL_20
#pragma config FPLLODIV =	DIV_1

#pragma config JTAGEN = OFF     
#pragma config FWDTEN = OFF    

 #define _XTAL_FREQ 8000000




void Timer4Setup()
{
    static int fTimerInitialised = 0;
    if(!fTimerInitialised)
    {        
                                          //  setup peripheral
      //  PR4 = 10000;                        //             set period register, generates one interrupt every 1 ms
        PR4 = 9850;
        TMR4 = 0;                           //             initialize count to 0
        T4CONbits.TCKPS = 3;                //            1:256 prescale value
        T4CONbits.TGATE = 0;                //             not gated input (the default)
        T4CONbits.TCS = 0;                  //             PCBLK input (the default)
        T4CONbits.ON = 1;                   //             turn on Timer1
        // PRIORITY WAS 2
        IPC4bits.T4IP = 2;                  //             priority
        IPC4bits.T4IS = 0;                  //             subpriority
        IFS0bits.T4IF = 0;                  //             clear interrupt flag
        IEC0bits.T4IE = 1;                  //             enable interrupt
        fTimerInitialised = 1;
    }
}

//==============================================================================
//Interrupt Handler- handled every 1msec
//==============================================================================
long currentTime = 0;
bool g_second_passed_flag = false;
void __ISR(_TIMER_4_VECTOR, ipl2auto) Timer4SR(void)
{
    currentTime++;
    if(currentTime % 10 == 0 || currentTime > 1000){
        increment_time_global();
    }
//    if(currentTime > 1000)
//        increment_time_global();
    IFS0bits.T4IF = 0;                  // clear interrupt flag
}

//==============================================================================
//this function initializes the needed components
//==============================================================================
char clean_line[LCD_LEN];
void init(){
    memset(clean_line,' ',LCD_LEN);
    LCD_Init(); 
    LED_Init();
    BTN_Init();
    SSD_Init(0);
    SWT_Init();
    AUDIO_Init(0);
    SSD_WriteDigits(0,0,0,0,0,0,1,0);
    RGBLED_Timer5Setup();
    Timer4Setup();
}



void main(){
    init(); //initialize all needed components
    MODE cur_mode;
    while((cur_mode = get_current_mode(SWT_GetGroupValue())) != INTERNAL_ERROR_MODE){
        IEC0bits.T3IE = 0; //disable the timer3 interrupts
       switch(cur_mode){
           case SHOW_TIME_MODE:
               SHOW_TIME();
               break;
           case SET_TIME_MODE:
           case ALARM_SET_MODE:
               SET_TIME(cur_mode);
               break;
           case UNKNOWN_MODE:
               SHOW_UNKNOWN_MESSAGE("BAD SWITCH COMBINATION");
               break;
       }
       LCD_WriteStringAtPos(clean_line, 0, 0);
    }
}






