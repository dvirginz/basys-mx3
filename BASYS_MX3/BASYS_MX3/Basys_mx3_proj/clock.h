
#ifndef _CLOCK_H    /* Guard against multiple inclusion */
#define _CLOCK_H


#include "utils.h"

extern int g_hours;
extern int g_minutes;
extern int g_seconds;
extern long currentTime;
extern bool g_second_passed_flag;

STATUS SHOW_TIME();
// Show the time on the LCD
//    Args:
//
//    Returns:
//          STATUS (enum defined in utils)
//
//      The function runs in a busy loop, and stops only if the buttons status is changed
//      or an unknown error has occurred

STATUS SET_TIME(MODE mode);
// Set the time on the LCD, blink screen each second
//    Args: mode - the current mode.
//
//    Returns:
//          STATUS (enum defined in utils)
//
//      The function runs in a busy loop, and stops only if the buttons status is changed
//      or an unknown error has occurred
//      sets the clock if mode==SET_TIME_MODE, if mode==ALARM_SET_MODE sets the alarm.

void increment_time_global( );
// Increment the time in the background
//    Args: 
//
//    Returns:
//          
//
//      The function runs in the background and increments the time each seconds

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
