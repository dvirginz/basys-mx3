
#ifndef _UTILS_H    /* Guard against multiple inclusion */
#define _UTILS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define TIME_STR_LEN 8
#define LCD_LEN 17

typedef enum{
    SET_TIME_MODE,
    SHOW_TIME_MODE,
    ALARM_SET_MODE,
    UNKNOWN_MODE,
    INTERNAL_ERROR_MODE,

}MODE;

typedef enum{
    STATUS_SUCCESS = 0,
    STATUS_UNKNOWN_FAILUE = -1,
    STATUS_ILLEGAL_ARGUMENT = 1
}STATUS;

extern char clean_line[LCD_LEN];

void DelayAprox10Us( unsigned int tusDelay );

void blink_screen(char * message,int start_pos_blink, int offset);
// blink the screen , make it to disappear for .1 of a second and then return
//    Args:
//      message - the message to blink and return
//      start_pos_blink - the start position of the blinker
//      int offset - specify how many digits to blink
//    Returns:     

void SHOW_UNKNOWN_MESSAGE(char * error_message);
// utility function to debug cases that should not happen
//    Args:
//      char * error_message - the message we wish to present
//    Returns:  

MODE get_current_mode(unsigned char bSwtVal);
// Get the mode of the MX3 by the switches in regard to the specifications.
//    Args:
//
//    Returns:
//      Returns the MODE (specified in the enum MODE), or -1 for error



#endif /* _UTILS_H */

/* *****************************************************************************
 End of File
 */
