#include "clock.h"
#include "ssd.h"
#include "led.h"
#include "audio.h"
#include <xc.h>
#define NUM_LEDS 8
#define ALARM_RING_INTERVAL 60
#define ALARM_SOUND_SWITCH_MASK (1<<7)
#define ALARM_LIGHT_SWITCH_MASK (1<<6)
// Default initialize clock to random variables
int g_hours = 23;
int g_minutes = 59;
int g_seconds = 55;
//Default initialize the alarm clock
int g_alarm_hours = 23;
int g_alarm_minutes = 59;
int g_alarm_seconds = 50;
int alarm_ring_count = ALARM_RING_INTERVAL + 1;
int alarm_curr_led_num = 0;
bool alarm_in_progress = false;
//Default random vals for stopper
int g_stopper_seconds = 0;
int g_stopper_10ms = 0;
bool stopper_10ms_flag = false;
bool g_10ms_flag = false;
bool stopper_active = false;

void increment_time_global() {

	if (currentTime % 10 == 0) {//10ms have passed. maintain counters for stopper
		g_10ms_flag = true;
		if (stopper_active) {
			g_stopper_10ms++;
			if (g_stopper_10ms == 100) {
				g_stopper_seconds++;
				g_stopper_10ms = 0;
			}
			if (g_stopper_seconds == 100) {
				g_stopper_seconds = g_stopper_10ms = 0;
			}
			stopper_10ms_flag = true;
		}

	}
	if (currentTime >= 999) {
		g_seconds++;

		if (g_seconds == 60) {
			g_minutes += 1;
			g_seconds = 0;
		}
		if (g_minutes == 60) {
			g_hours += 1;
			g_minutes = 0;
		}
		if (g_hours == 24) {
			g_hours = 0;
			g_minutes = 0;
			g_seconds = 0;
		}
		g_second_passed_flag = true;
		currentTime = 0;
	}

}

STATUS SHOW_TIME() {
	//int alarm_ring_time = 3600*g_alarm_hours + 60 * g_alarm_minutes + g_alarm_seconds;
	//stopper_active = true;
	char time_str[LCD_LEN];
	memset(time_str, 0, LCD_LEN);
	unsigned char bSwtVal = SWT_GetGroupValue();
	MODE cur_mode;
	bool wait_for_reset = false;
	bool wait_for_pause = false;
	while ((cur_mode = get_current_mode(bSwtVal)) == SHOW_TIME_MODE) {
		if (stopper_active) {// we need to update the stopper _flag && 
							 //write on the SSD.
			SSD_WriteDigits(g_stopper_10ms % 10, g_stopper_10ms / 10, g_stopper_seconds % 10, g_stopper_seconds / 10, 0, 0, 1, 0);
		}
		if (BTN_GetValue('L')) wait_for_reset = true;
		if (wait_for_reset && !BTN_GetValue('L')) {
			wait_for_reset = false;
			g_stopper_seconds = g_stopper_10ms = 0;
			SSD_WriteDigits(g_stopper_10ms % 10, g_stopper_10ms / 10, g_stopper_seconds % 10, g_stopper_seconds / 10, 0, 0, 1, 0);
		}
		if (BTN_GetValue('R')) wait_for_pause = true;//start/stop the stopper
		if (wait_for_pause && !BTN_GetValue('R')) {
			wait_for_pause = false;
			stopper_active = !stopper_active;
		}
		if ((currentTime / 100) % 2 == 0) {
			//print time in HH : MM : SS format
			memset(time_str, 0, LCD_LEN);
			sprintf(time_str, "%02d:%02d:%02d", g_hours, g_minutes, g_seconds);
			LCD_WriteStringAtPos(time_str, 0, 0);
		}
		if (g_10ms_flag) {//g_second_passed_flag
						  //g_second_passed_flag = false;
			g_10ms_flag = false;
			//check the alarm clock
			if (g_hours == g_alarm_hours && g_minutes == g_alarm_minutes && g_seconds == g_alarm_seconds) {
				alarm_ring_count = 0;
				//                alarm_in_progress = true;
			}
			//int curr_time_sec = (3600*g_hours + 60 * g_minutes + g_seconds);
			if (alarm_ring_count < ALARM_RING_INTERVAL) {//&& (SWT_GetValue(ALARM_LIGHT_SWITCH)==1
														 //print time in HH : MM : SS format
				memset(time_str, 0, LCD_LEN);
				sprintf(time_str, "%02d:%02d:%02d", g_hours, g_minutes, g_seconds);
				LCD_WriteStringAtPos(time_str, 0, 0);
				if (bSwtVal & ALARM_LIGHT_SWITCH_MASK) {
					LED_SetGroupValue((1 << alarm_curr_led_num));
				}
				else {
					LED_SetGroupValue((0));
				}
				if (bSwtVal & ALARM_SOUND_SWITCH_MASK) {
					IEC0bits.T3IE = 1;
					//OC1RS = 1000 - (100)*(g_seconds%2);
				}
				else {
					IEC0bits.T3IE = 0;
				}
				//LED_SetValue(alarm_prev_led_num,0);
				//LED_SetValue(alarm_curr_led_num,1);
				// SSD_WriteDigits(alarm_curr_led_num,0,alarm_ring_count%10,alarm_ring_count/10,0,0,1,0);
				if (g_second_passed_flag) {
					alarm_ring_count++;
					if ((alarm_ring_count / (NUM_LEDS - 1)) % 2 == 0) {
						alarm_curr_led_num++;
					}
					else {
						alarm_curr_led_num--;
					}
					//alarm_ring_count++;
					// g_second_passed_flag =false;
				}
			}
			else {
				LED_SetGroupValue(0);
				IEC0bits.T3IE = 0;
			}
		}
		bSwtVal = SWT_GetGroupValue();
		if (g_second_passed_flag) g_second_passed_flag = false;
	}
	stopper_active = false;
	return STATUS_SUCCESS;
}
#define DEFAULT_DELAY_FACTOR 1000
#define CONSECUTIVE_THRESHOLD 3
STATUS SET_TIME(MODE mode) {
	int cur_hour, cur_min, cur_sec;

	char time_str[LCD_LEN];
	memset(time_str, 0, LCD_LEN);

	if (mode == SET_TIME_MODE) {
		cur_hour = g_hours, cur_min = g_minutes, cur_sec = g_seconds;
		sprintf(time_str, "SET %02d:%02d:%02d", cur_hour, cur_min, cur_sec);
		LCD_WriteStringAtPos(time_str, 0, 0);
	}
	else if (mode == ALARM_SET_MODE) {
		cur_hour = g_alarm_hours, cur_min = g_alarm_minutes, cur_sec = g_alarm_seconds;
		sprintf(time_str, "ALARM %02d:%02d:%02d", cur_hour, cur_min, cur_sec);
		LCD_WriteStringAtPos(time_str, 0, 0);
	}
	else {
		return STATUS_ILLEGAL_ARGUMENT;
	}

	MODE cur_mode;
	int btnc_counter = 0;
	bool btnc_is_down = false;
	int consecutive_up = 0, consecutive_down = 0;
	//at start, wait for .1 seconds
	int delay_factor = DEFAULT_DELAY_FACTOR;

	while ((cur_mode = get_current_mode(SWT_GetGroupValue())) == mode) {
		if (g_second_passed_flag && btnc_counter > 0 && consecutive_up == 0 && consecutive_down == 0) {
			// X + (btnc_counter - 1) * 3 represents the start position 
			//      for the relevant time frame to blink
			// 2 represents the number of digits to blink

			if (mode == SET_TIME_MODE) {
				blink_screen(time_str, 4 + (btnc_counter - 1) * 3, 2);
			}
			else if (mode == ALARM_SET_MODE) {
				blink_screen(time_str, 6 + (btnc_counter - 1) * 3, 2);
			}
			g_second_passed_flag = false;
		}
		if (BTN_GetValue('C') && !btnc_is_down) {
			btnc_counter = (btnc_counter + 1) % 4;
			consecutive_up = consecutive_down = 0;
			btnc_is_down = true;
		}
		else if (!BTN_GetValue('C')) {
			btnc_is_down = false;
		}
		if (BTN_GetValue('R')) {

			switch (btnc_counter) {
			case(1):
				cur_hour++;
				break;
			case(2):
				cur_min++;
				break;
			case(3):
				cur_sec++;
				break;
			}
			if (consecutive_up)
				DelayAprox10Us(delay_factor);
			consecutive_down = 0;
			consecutive_up++;
			if (consecutive_up > CONSECUTIVE_THRESHOLD)
				delay_factor /= 2;

		}

		else if (BTN_GetValue('L')) {

			switch (btnc_counter) {
			case(1):
				cur_hour--;
				break;
			case(2):
				cur_min--;
				break;
			case(3):
				cur_sec--;
				break;
			}
			if (consecutive_down)
				DelayAprox10Us(delay_factor);
			consecutive_up = 0;
			consecutive_down++;
			if (consecutive_down > CONSECUTIVE_THRESHOLD)
				delay_factor /= 2;
		}
		else {
			consecutive_up = consecutive_down = 0;
		}
		if (consecutive_down <= CONSECUTIVE_THRESHOLD && consecutive_up <= CONSECUTIVE_THRESHOLD) {
			delay_factor = DEFAULT_DELAY_FACTOR;
		}

		if (cur_sec == 60) {
			cur_sec = 0;
		}
		if (cur_min == 60) {
			cur_min = 0;
		}
		if (cur_hour == 24) {
			cur_hour = 0;
		}
		if (cur_sec == -1) {
			cur_sec = 59;
		}
		if (cur_min == -1) {
			cur_min = 59;
		}
		if (cur_hour == -1) {
			cur_hour = 23;
		}
		if (mode == SET_TIME_MODE) {
			sprintf(time_str, "SET %02d:%02d:%02d", cur_hour, cur_min, cur_sec);
			LCD_WriteStringAtPos(time_str, 0, 0);
		}
		else if (mode == ALARM_SET_MODE) {
			sprintf(time_str, "ALARM %02d:%02d:%02d", cur_hour, cur_min, cur_sec);
			LCD_WriteStringAtPos(time_str, 0, 0);
		}

	}
	if (mode == SET_TIME_MODE) {
		g_hours = cur_hour, g_minutes = cur_min, g_seconds = cur_sec;
	}
	else {
		g_alarm_hours = cur_hour, g_alarm_minutes = cur_min, g_alarm_seconds = cur_sec;
	}
}