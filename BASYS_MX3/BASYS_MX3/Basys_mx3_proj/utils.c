/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Digilent

  @File Name
    utils.c

  @Description
        This library implements the delay functionality used in other libraries.  
        The delay is implemented using loop, so the delay time is not exact. 
        For exact timing use timers.
        Include the file in the project, together with utils.h, when this library is needed	
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
#include <xc.h>
#include <sys/attribs.h>
#include "config.h"
#include "utils.h"
#include "swt.h"
/* ************************************************************************** */

/* ------------------------------------------------------------ */
/***    Delay100Us
**
**	Synopsis:
**		Delay100Us(100)
**
**	Parameters:
**		t100usDelay - the amount of time you wish to delay in hundreds of microseconds
**
**	Return Values:
**      none
**
**	Errors:
**		none
**
**	Description:
**		This procedure delays program execution for the specified number
**      of microseconds. This delay is not precise.
**		
**	Note:
**		This routine is written with the assumption that the
**		system clock is 40 MHz.
*/
void DelayAprox10Us( unsigned int  t100usDelay )
{
    int j;
    while ( 0 < t100usDelay )
    {
        t100usDelay--;
        j = 14;
        while ( 0 < j )
        {
            j--;
        }   // end while 
        asm volatile("nop"); // do nothing
        asm volatile("nop"); // do nothing
        asm volatile("nop"); // do nothing
        asm volatile("nop"); // do nothing
        asm volatile("nop"); // do nothing
         
    }   // end while
}


void blink_screen(char * message, int start_pos_blink, int offset){
    char blinked_message[LCD_LEN];
    strncpy(blinked_message,message,LCD_LEN);
    strncpy(blinked_message + start_pos_blink, clean_line,offset);
    LCD_WriteStringAtPos(blinked_message, 0, 0);
    LCD_WriteStringAtPos(message, 0, 0);
}

MODE get_current_mode(unsigned char bSwtVal){
//    unsigned char bSwtVal;
//    bSwtVal = SWT_GetGroupValue();
    //LED_SetGroupValue(bSwtVal);
    //bitwise operation to check if first two switches are off
    if ((bSwtVal & 3) == 0) {
        return SHOW_TIME_MODE;
    }
    else if ((bSwtVal & 3) == 1) {
    	return SET_TIME_MODE;
    }
    else if ((bSwtVal & 3) == 2) {
    	return ALARM_SET_MODE;
    }
    else{
        return UNKNOWN_MODE;
    }
    //TODO INSERT OTHER MODES
}

void SHOW_UNKNOWN_MESSAGE(char * error_message){
    LCD_WriteStringAtPos(error_message, 0, 0);
    DelayAprox10Us(1000);
}
/* *****************************************************************************
 End of File
 */
